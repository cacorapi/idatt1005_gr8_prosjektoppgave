package idi.ntnu.no.idatt1005projektoppgave.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IngredientItemTest {
  IngredientItem item;

  @BeforeEach
  void setUp() {
    item = new IngredientItem(0, "Custom", 1, 0, IngredientItem.Location.inventory);
  }

  @Test
  void getId() {
    assertEquals(item.getId(), 0);
  }

  @Test
  void getIngredient() {
    assertNull(item.getIngredient().orElse(null));
  }

  @Test
  void getCustomName() {
    assertEquals(item.getCustomName().orElse(null), "Custom");
  }

  @Test
  void getAmount() {
    assertEquals(item.getAmount(), 1);
  }

  @Test
  void getPlannedRecipeId() {
    assertEquals(item.getPlannedRecipeId(), 0);
  }

  @Test
  void isCustom() {
    assertTrue(item.isCustom());
  }

  @Test
  void getName() {
    assertEquals(item.getName(), "Custom");
  }

  @Test
  void getLocation() {
    assertEquals(item.getLocation(), IngredientItem.Location.inventory);
  }
}
