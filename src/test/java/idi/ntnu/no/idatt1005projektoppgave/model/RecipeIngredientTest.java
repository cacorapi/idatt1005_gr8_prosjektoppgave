package idi.ntnu.no.idatt1005projektoppgave.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RecipeIngredientTest {
  RecipeIngredient recipeIngredient;

  @BeforeEach
  void setUp() {
    recipeIngredient = new RecipeIngredient(1, "customName", 1, true);
  }

  @Test
  void getAmount() {
    assertEquals(1, recipeIngredient.getAmount());
  }

  @Test
  void isNecessary() {
    assertTrue(recipeIngredient.isNecessary());
  }

  @Test
  void getId() {
    assertEquals(1, recipeIngredient.getId());
  }

  @Test
  void getCustomName() {
    assertEquals("customName", recipeIngredient.getCustomName().orElse(null));
  }

  @Test
  void getIngredient() {
    assertNull(recipeIngredient.getIngredient().orElse(null));
  }

  @Test
  void getName() {
    assertEquals("customName", recipeIngredient.getName());
  }
}
