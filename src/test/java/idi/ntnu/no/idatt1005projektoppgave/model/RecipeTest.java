package idi.ntnu.no.idatt1005projektoppgave.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RecipeTest {
  Recipe recipe;

  @BeforeEach
  void setUp() {
    recipe = new Recipe(1, List.of("Step 1", "Step 2"), Recipe.Category.breakfast,
        Duration.ofMinutes(30), Recipe.Difficulty.beginner, "Recipe name", "Recipe description",
        false, "Note", List.of(new RecipeIngredient(1, "Ingredient", 1, false)));
  }

  @Test
  void getId() {
    assertEquals(1, recipe.getId());
  }

  @Test
  void getSteps() {
    assertEquals(List.of("Step 1", "Step 2"), recipe.getSteps());
  }

  @Test
  void getCategory() {
    assertEquals(Recipe.Category.breakfast, recipe.getCategory());
  }

  @Test
  void getDuration() {
    assertEquals(Duration.ofMinutes(30), recipe.getDuration());
  }

  @Test
  void getDifficulty() {
    assertEquals(Recipe.Difficulty.beginner, recipe.getDifficulty());
  }

  @Test
  void getName() {
    assertEquals("Recipe name", recipe.getName());
  }

  @Test
  void getDescription() {
    assertEquals("Recipe description", recipe.getDescription());
  }

  @Test
  void isFavorite() {
    Assertions.assertFalse(recipe.isFavorite());
  }

  @Test
  void getNote() {
    assertEquals("Note", recipe.getNote());
  }

  @Test
  void getIngredients() {
    assertEquals(recipe.getIngredients().size(), 1);
  }
}
