package idi.ntnu.no.idatt1005projektoppgave.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IngredientTest {

  Ingredient ingredient;

  @BeforeEach
  void setUp() {
    ingredient = new Ingredient(0, "Test", Ingredient.MeasuringUnit.gram, Ingredient.Category.fruit,
        Ingredient.StorageType.pantry);
  }

  @Test
  void getId() {
    assert ingredient.getId() == 0;
  }

  @Test
  void getName() {
    assert ingredient.getName().equals("Test");
  }

  @Test
  void getUnit() {
    assert ingredient.getUnit().equals(Ingredient.MeasuringUnit.gram);
  }

  @Test
  void getCategory() {
    assert ingredient.getCategory().equals(Ingredient.Category.fruit);
  }

  @Test
  void getStorageType() {
    assert ingredient.getStorageType().equals(Ingredient.StorageType.pantry);
  }
}
