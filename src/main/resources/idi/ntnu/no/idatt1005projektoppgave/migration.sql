PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS ingredient
(
    id           INTEGER
        PRIMARY KEY AUTOINCREMENT,
    name         VARCHAR,
    unit         TEXT CHECK (unit IN ('milliliter', 'gram', 'teaspoon', 'spoon', 'piece')),
    category     TEXT CHECK (category IN
                             ('fruit', 'vegetable', 'meat', 'dairy', 'grain', 'herb', 'spice',
                              'oil')),
    storage_type TEXT CHECK (storage_type IN ('fridge', 'freezer', 'pantry'))
);

CREATE TABLE IF NOT EXISTS recipe
(
    id               INTEGER
        PRIMARY KEY AUTOINCREMENT,
    steps            VARCHAR,
    meal_category    TEXT CHECK (meal_category IN
                                 ('breakfast', 'lunch', 'dinner', 'dessert')),
    duration_minutes INT,
    difficulty       TEXT CHECK (difficulty IN ('beginner', 'intermediate', 'expert')),
    name             VARCHAR,
    description      TEXT,
    is_favorite      BOOLEAN,
    note             TEXT
);

CREATE TABLE IF NOT EXISTS ingredient_item
(
    id                 INTEGER PRIMARY KEY AUTOINCREMENT,
    ingredient_id      INTEGER NULL,
    amount             INTEGER,
    custom_name        VARCHAR NULL,
    recipe_id INTEGER NULL,
    location          TEXT CHECK (location IN ('inventory', 'shoppingList')),
    FOREIGN KEY (ingredient_id) REFERENCES ingredient (id),
    FOREIGN KEY (recipe_id) REFERENCES recipe (id)
);

CREATE TABLE IF NOT EXISTS recipe_ingredient_relation
(
    ingredient_id INTEGER,
    recipe_id     INTEGER,
    amount        INT,
    is_necessary  BOOLEAN,
    FOREIGN KEY (ingredient_id) REFERENCES ingredient (id),
    FOREIGN KEY (recipe_id) REFERENCES recipe (id),
    PRIMARY KEY (ingredient_id, recipe_id)
);

INSERT INTO ingredient (name, unit, category, storage_type)
VALUES ('Apple', 'piece', 'fruit', 'fridge'),
       ('Potato', 'gram', 'vegetable', 'pantry'),
       ('Chicken Breast', 'gram', 'meat', 'freezer'),
       ('Cheddar Cheese', 'gram', 'dairy', 'fridge'),
       ('Rice', 'gram', 'grain', 'pantry'),
       ('Basil', 'teaspoon', 'herb', 'fridge'),
       ('Black Pepper', 'teaspoon', 'spice', 'pantry'),
       ('Olive Oil', 'milliliter', 'oil', 'pantry'),
       ('Noodles', 'gram', 'grain', 'pantry'),
       ('Ground Beef', 'gram', 'meat', 'freezer'),
       ('Lettuce', 'gram', 'vegetable', 'fridge'),
       ('Salt', 'teaspoon', 'spice', 'pantry'),
       ('Broccoli', 'piece', 'vegetable', 'fridge'),
       ('Flour', 'gram', 'grain', 'pantry'),
       ('Eggs', 'piece', 'dairy', 'fridge'),
       ('Tomato', 'piece', 'vegetable', 'fridge'),
       ('Onion', 'piece', 'vegetable', 'pantry'),
       ('Garlic', 'piece', 'vegetable', 'pantry'),
       ('Ground Turkey', 'gram', 'meat', 'freezer'),
       ('Milk', 'milliliter', 'dairy', 'fridge'),
       ('Carrot', 'gram', 'vegetable', 'fridge'),
       ('Parmesan Cheese', 'gram', 'dairy', 'fridge'),
       ('Pasta', 'gram', 'grain', 'pantry'),
       ('Soy Sauce', 'milliliter', 'spice', 'pantry'),
       ('Ground Pork', 'gram', 'meat', 'freezer'),
       ('Green Beans', 'gram', 'vegetable', 'fridge'),
       ('Sugar', 'gram', 'spice', 'pantry'),
       ('Avocado', 'piece', 'vegetable', 'fridge'),
       ('Bell Pepper', 'piece', 'vegetable', 'fridge'),
       ('Spinach', 'gram', 'vegetable', 'fridge'),
       ('Cabbage', 'gram', 'vegetable', 'fridge'),
       ('Shrimp', 'gram', 'meat', 'freezer'),
       ('Salmon', 'gram', 'meat', 'freezer'),
       ('Lemon', 'piece', 'fruit', 'fridge'),
       ('Lime', 'piece', 'fruit', 'fridge'),
       ('Cucumber', 'piece', 'vegetable', 'fridge'),
       ('Pineapple', 'piece', 'fruit', 'fridge'),
       ('Blueberry', 'gram', 'fruit', 'fridge'),
       ('Raspberry', 'gram', 'fruit', 'fridge'),
       ('Mango', 'piece', 'fruit', 'fridge');

INSERT INTO recipe (steps, meal_category, duration_minutes, difficulty, name, description, is_favorite, note)
VALUES ('1. Prepare ingredients; 2. Cook; 3. Serve', 'dinner', 30, 'beginner', 'Grilled Chicken', 'Simple grilled chicken recipe', 1, 'Family favorite');

INSERT INTO recipe (steps, meal_category, duration_minutes, difficulty, name, description, is_favorite, note)
VALUES ('1. Chop vegetables; 2. Boil water; 3. Cook rice; 4. Mix everything', 'lunch', 45, 'intermediate', 'Vegetable Fried Rice', 'Healthy and tasty vegetable fried rice', 0, 'Add extra soy sauce for flavor');

INSERT INTO recipe (steps, meal_category, duration_minutes, difficulty, name, description, is_favorite, note)
VALUES ('1. Eat them raw.', 'breakfast', 0, 'beginner', 'Noodles', 'A meal fit for the student lifestyle.', 1, 'crunch');

INSERT INTO recipe (steps, meal_category, duration_minutes, difficulty, name, description, is_favorite, note)
VALUES ('1. Salt the lettuce.', 'dessert', 5, 'beginner', 'Salty lettuce', 'Deliciously salty!', 1, 'It uhh... hmm it uhh...');

INSERT INTO recipe_ingredient_relation (ingredient_id, recipe_id, amount, is_necessary)
VALUES (3, 1, 200, 1),
       (7, 1, 5, 0),
       (1, 2, 150, 1),
       (2, 2, 200, 1),
       (5, 2, 250, 1),
       (8, 2, 20, 1),
       (9, 3, 200, 1),
       (11, 4, 100, 1),
       (12, 4, 2, 1);