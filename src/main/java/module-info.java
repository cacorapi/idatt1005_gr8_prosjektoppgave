module idi.ntnu.no.idatt1005_gr8_prosjektoppgave {
  requires javafx.controls;
  requires javafx.fxml;

  requires net.synedra.validatorfx;
  requires org.kordamp.ikonli.javafx;
  requires org.kordamp.bootstrapfx.core;
  requires java.sql;
  requires org.xerial.sqlitejdbc;
  requires org.mybatis;
  requires org.controlsfx.controls;
  requires resend.java;

  opens idi.ntnu.no.idatt1005projektoppgave to javafx.fxml;
  exports idi.ntnu.no.idatt1005projektoppgave;
  exports idi.ntnu.no.idatt1005projektoppgave.model;
  opens idi.ntnu.no.idatt1005projektoppgave.model to javafx.fxml;
  exports idi.ntnu.no.idatt1005projektoppgave.database;
  opens idi.ntnu.no.idatt1005projektoppgave.database to javafx.fxml;
  exports idi.ntnu.no.idatt1005projektoppgave.view.confirmDialog;
  opens idi.ntnu.no.idatt1005projektoppgave.view.confirmDialog to javafx.fxml;
}
