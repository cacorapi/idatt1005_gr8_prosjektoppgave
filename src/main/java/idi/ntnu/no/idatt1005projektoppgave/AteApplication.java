package idi.ntnu.no.idatt1005projektoppgave;

import idi.ntnu.no.idatt1005projektoppgave.database.LocalDbSource;
import idi.ntnu.no.idatt1005projektoppgave.database.SqliteDatabase;
import idi.ntnu.no.idatt1005projektoppgave.database.TestingDbSource;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The main entry point for the application.
 */
public class AteApplication extends Application {
  @Override
  public void start(Stage stage) throws IOException {
    try {
      var db = new SqliteDatabase(new TestingDbSource());
      db.getIngredient(1);
    } catch (Exception ignored) {
      try { // the database isn't migrated, let's try
        var db = new SqliteDatabase(new TestingDbSource());
        System.out.println("Migrating");
        db.migrate();
      } catch (Exception ignored1) {
        // migrations failed, but the user should still
        // be able to look around the app
      }
    }

    FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("app-view.fxml"));
    Scene scene = new Scene(fxmlLoader.load(), 782, 536);
    stage.setTitle("ATE");
    stage.setScene(scene);
    stage.setResizable(false);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }
}
