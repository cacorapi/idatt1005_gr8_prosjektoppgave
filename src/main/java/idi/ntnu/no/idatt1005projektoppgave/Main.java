package idi.ntnu.no.idatt1005projektoppgave;

import javafx.application.Application;

/**
 * Contains the entry point of the application.
 */
public class Main {

  /**
   * The entry point of the application.
   */
  public static void main(String[] args) {
    Application.launch(AteApplication.class, args);
  }
}
