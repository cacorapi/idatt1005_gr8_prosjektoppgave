package idi.ntnu.no.idatt1005projektoppgave;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Resource {
  public static String asString(String path) throws IOException {
    var is = AteApplication.class.getResourceAsStream(path);
   BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    String line;
    StringBuilder sb = new StringBuilder();
    while ((line = reader.readLine()) != null) {
      sb.append(line).append("\n");
    }
    return sb.toString();
  }
}
