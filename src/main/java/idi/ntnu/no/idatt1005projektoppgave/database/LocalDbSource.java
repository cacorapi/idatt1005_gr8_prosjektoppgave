package idi.ntnu.no.idatt1005projektoppgave.database;

import idi.ntnu.no.idatt1005projektoppgave.AteApplication;
import java.util.Objects;

/**
 * A local SQLite database source.
 */
public class LocalDbSource implements DbSource {
  @Override
  public String getConnectionUrl() {
    return "jdbc:sqlite:./data.db";
  }
}
