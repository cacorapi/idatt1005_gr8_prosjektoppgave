package idi.ntnu.no.idatt1005projektoppgave.database;

import idi.ntnu.no.idatt1005projektoppgave.AteApplication;
import idi.ntnu.no.idatt1005projektoppgave.model.Ingredient;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import idi.ntnu.no.idatt1005projektoppgave.model.Recipe;
import idi.ntnu.no.idatt1005projektoppgave.model.RecipeIngredient;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * A SQLite database implementation.
 */
public class SqliteDatabase implements Database {
  private Connection connection;
  private final DbSource source;

  private void openConnection() throws SQLException {
    // Open a connection to the database, and store it in this instance
    this.connection = DriverManager.getConnection(source.getConnectionUrl());
  }

  private void closeConnection() throws SQLException {
    this.connection.close();
  }

  private void closeStatement(PreparedStatement statement) throws SQLException {
    statement.close();
    this.connection.close();
  }

  public SqliteDatabase(DbSource source)
      throws SQLException, ClassNotFoundException, NullPointerException {
    this.source = source;
    // Check that the drivers are healthy, and that the database is available
    checkDriversHealthy();
  }

  private static void checkDriversHealthy() throws ClassNotFoundException, SQLException {
    // Ensure that the JDBC class is loaded
    Class.forName("org.sqlite.JDBC");
    // Register the driver in the Driver manager, so we can communicate with sqlite
    DriverManager.registerDriver(new org.sqlite.JDBC());
  }

  @Override
  public void migrate() throws FileNotFoundException, SQLException {
    openConnection();
    // Connect a script runner to this database
    ScriptRunner runner = new ScriptRunner(this.connection);

    // Get access to the migration script from file
    String migrationScriptLocation =
        Objects.requireNonNull(AteApplication.class.getResource("migration.sql")).toExternalForm();

    // Read and run the migration script
    Reader scriptReader = new BufferedReader(new FileReader(migrationScriptLocation));
    runner.runScript(scriptReader);

    // Try to fetch item, if none are present, populate the database with initial values
    // this is a simple test to check if the database is populated, and if not, populate it
    try {
      this.getIngredient(0);
    } catch (Exception e) {
      System.out.println("Populating");
      // populate
      String populateScriptLocation =
          Objects.requireNonNull(AteApplication.class.getResource("populate.sql")).toExternalForm();
      runner.runScript(new FileReader(populateScriptLocation));
    }

    closeConnection();
  }

  @Override
  public Ingredient getIngredient(int ingredientId) throws SQLException {
    openConnection();

    String sql = "SELECT * FROM ingredient WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, ingredientId);
    ResultSet result = statement.executeQuery();

    var ingredients = Ingredient.fromDatabase(result);
    closeStatement(statement);
    return ingredients;
  }

  @Override
  public List<Ingredient> getAllIngredients() throws SQLException {
    openConnection();
    String sql = "SELECT * FROM ingredient";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    ResultSet result = statement.executeQuery();

    ArrayList<Ingredient> ingredients = new ArrayList<>();
    // Map through all results using the result iterator
    while (result.next()) {
      ingredients.add(Ingredient.fromDatabase(result));
    }

    closeStatement(statement);
    return ingredients;
  }

  @Override
  public void updateIngredient(Ingredient newIngredient) throws SQLException {
    openConnection();
    String sql = "UPDATE ingredient SET name = ?, unit = ?, category = ?, " +
        "storage_type = ? WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);

    statement.setString(1, newIngredient.getName());
    statement.setString(2, newIngredient.getUnit().name());
    statement.setString(3, newIngredient.getCategory().name());
    statement.setString(4, newIngredient.getStorageType().name());
    statement.setInt(5, newIngredient.getId());

    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void deleteIngredient(int ingredientId) throws SQLException {
    openConnection();
    String sql = "DELETE FROM ingredient WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);

    statement.setInt(1, ingredientId);
    statement.execute();
    closeStatement(statement);
  }

  @Override
  public Recipe getRecipe(int recipeId) throws SQLException {
    openConnection();
    String sql = "SELECT * FROM recipe WHERE recipe.id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);

    statement.setInt(1, recipeId);
    ResultSet result = statement.executeQuery();

    List<RecipeIngredient> ingredients = getRecipeIngredientsForRecipe(recipeId);

    Recipe recipe = Recipe.fromDatabase(recipeId, result, ingredients);
    closeStatement(statement);
    return recipe;
  }

  private List<RecipeIngredient> getRecipeIngredientsForRecipe(int recipeId) throws SQLException {
    openConnection();
    String sql =
        "SELECT * FROM recipe_ingredient_relation JOIN ingredient i  ON i.id = recipe_ingredient_relation.ingredient_id WHERE recipe_id = ? ";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, recipeId);
    ResultSet result = statement.executeQuery();

    ArrayList<RecipeIngredient> ingredients = new ArrayList<>();
    while (result.next()) {
      ingredients.add(RecipeIngredient.fromDatabase(result));
    }

    closeStatement(statement);
    return ingredients;
  }


  @Override
  public List<Recipe> getAllRecipes() throws SQLException {
    openConnection();
    String sql = "SELECT * FROM recipe";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    ResultSet result = statement.executeQuery();

    List<Recipe> recipes = new ArrayList<>();
    while (result.next()) {
      int recipeId = result.getInt("id");
      List<RecipeIngredient> ingredients = getRecipeIngredientsForRecipe(recipeId);
      recipes.add(Recipe.fromDatabase(recipeId, result, ingredients));
    }

    closeStatement(statement);
    return recipes;
  }

  @Override
  public void updateRecipeNote(int recipeId, String note) throws SQLException {
    openConnection();
    String sql = "UPDATE recipe SET note = ? WHERE id = ? RETURNING *";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, note);
    statement.setInt(2, recipeId);
    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void updateRecipeFavorite(int recipeId, boolean isFavorite) throws SQLException {
    openConnection();
    String sql = "UPDATE recipe SET is_favorite = ? WHERE id = ? RETURNING *";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setBoolean(1, isFavorite);
    statement.setInt(2, recipeId);
    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void addIngredientItem(Ingredient ingredient, int amount, int plannedRecipeId,
                                IngredientItem.Location location) throws SQLException {
    openConnection();
    String sql =
        "INSERT INTO ingredient_item (ingredient_id, amount, recipe_id, location) " +
            "VALUES (?, ?, ?, ?)";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, ingredient.getId());
    statement.setInt(2, amount);
    if (plannedRecipeId == -1) {
      statement.setNull(3, java.sql.Types.INTEGER);
    } else {
      statement.setInt(3, plannedRecipeId);
    }
    statement.setString(4, location.name());

    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void addCustomIngredientItem(String customItemName, int amount,
                                      IngredientItem.Location location) throws SQLException {
    openConnection();
    String sql = "INSERT INTO ingredient_item (custom_name, amount, location) VALUES (?, ?, ?)";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, customItemName);
    statement.setInt(2, amount);
    statement.setString(3, location.name());

    statement.execute();
    closeStatement(statement);
  }


  @Override
  public void deleteIngredientItem(int ingredientItemId) throws SQLException {
    openConnection();
    String sql = "DELETE FROM ingredient_item WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, ingredientItemId);
    statement.execute();
    closeStatement(statement);
  }


  @Override
  public List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location)
      throws SQLException {
    openConnection();
    String sql =
        "SELECT * FROM ingredient_item LEFT JOIN main.ingredient i ON i.id = " + "ingredient_item" +
            ".ingredient_id WHERE location = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, location.name());
    ResultSet result = statement.executeQuery();

    ArrayList<IngredientItem> shoppingListItems = new ArrayList<>();
    while (result.next()) {
      shoppingListItems.add(IngredientItem.fromDatabase(result));
    }

    closeStatement(statement);
    return shoppingListItems;
  }

  @Override
  public List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location,
                                                              Ingredient.StorageType storageType)
      throws SQLException {
    openConnection();
    String sql =
        "SELECT * FROM ingredient_item LEFT JOIN main.ingredient i ON i.id = " + "ingredient_item" +
            ".ingredient_id WHERE location = ? AND i.storage_type = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, location.name());
    statement.setString(2, storageType.name());
    ResultSet result = statement.executeQuery();

    ArrayList<IngredientItem> shoppingListItems = new ArrayList<>();
    while (result.next()) {
      shoppingListItems.add(IngredientItem.fromDatabase(result));
    }

    closeStatement(statement);
    return shoppingListItems;
  }

  @Override
  public List<IngredientItem> getAllCustomInventoryItems() throws SQLException {
    openConnection();
    String sql =
        "SELECT * FROM ingredient_item WHERE location = 'inventory' AND custom_name IS NOT NULL";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    ResultSet result = statement.executeQuery();

    ArrayList<IngredientItem> customItems = new ArrayList<>();
    while (result.next()) {
      customItems.add(IngredientItem.fromDatabase(result));
    }

    closeStatement(statement);
    return customItems;
  }

  @Override
  public void updateIngredientItemAmount(int ingredientItemId, int newCount) throws SQLException {
    openConnection();
    String sql = "UPDATE ingredient_item SET amount = ? WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, newCount);
    statement.setInt(2, ingredientItemId);

    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void moveShoppingListToInventory() throws SQLException {
    openConnection();
    String sql =
        "UPDATE ingredient_item SET location = 'inventory' WHERE location = " + "'shoppingList'";

    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.execute();
    closeStatement(statement);
  }

  @Override
  public void moveIngredientItemToLocation(int itemId, IngredientItem.Location location)
      throws SQLException {
    openConnection();
    String sql = "UPDATE ingredient_item SET location = ? WHERE id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, location.name());
    statement.setInt(2, itemId);
    statement.execute();
    closeStatement(statement);
  }

  @Override
  public List<Ingredient> getIngredientsByStorageType(Ingredient.StorageType storageType)
      throws SQLException {
    openConnection();
    String sql = "SELECT * FROM ingredient WHERE ingredient.storage_type = ?";

    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setString(1, storageType.name());
    ResultSet result = statement.executeQuery();

    ArrayList<Ingredient> inventoryItems = new ArrayList<>();
    while (result.next()) {
      inventoryItems.add(Ingredient.fromDatabase(result));
    }

    closeStatement(statement);
    return inventoryItems;
  }

  @Override
  public void removeIngredientItemsOfRecipe(int recipeId) throws SQLException {
    openConnection();
    String sql = "DELETE FROM ingredient_item WHERE recipe_id = ?";
    PreparedStatement statement = this.connection.prepareStatement(sql);
    statement.setInt(1, recipeId);
    statement.execute();
    closeStatement(statement);
  }
}
