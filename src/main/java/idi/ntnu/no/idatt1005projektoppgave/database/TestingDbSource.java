package idi.ntnu.no.idatt1005projektoppgave.database;

/**
 * A testing database source that can be run by unit tests.
 */
public class TestingDbSource implements DbSource {
  @Override
  public String getConnectionUrl() {
    return "jdbc:sqlite:test.sqlite";
  }
}
