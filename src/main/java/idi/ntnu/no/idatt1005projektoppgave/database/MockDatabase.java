package idi.ntnu.no.idatt1005projektoppgave.database;

import idi.ntnu.no.idatt1005projektoppgave.model.Ingredient;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import idi.ntnu.no.idatt1005projektoppgave.model.Recipe;
import idi.ntnu.no.idatt1005projektoppgave.model.RecipeIngredient;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * A mock database for the purposes of testing.
 */
public class MockDatabase implements Database {
  private static final Ingredient carrots =
      new Ingredient(15, "Carrots", Ingredient.MeasuringUnit.piece, Ingredient.Category.vegetable,
          Ingredient.StorageType.fridge);
  private static final Ingredient sausage =
      new Ingredient(12, "Sausage", Ingredient.MeasuringUnit.piece, Ingredient.Category.meat,
          Ingredient.StorageType.fridge);
  private static final Ingredient battery =
      new Ingredient(99, "Battery Energy Drink", Ingredient.MeasuringUnit.milliliter,
          Ingredient.Category.dairy, Ingredient.StorageType.pantry);
  private static final RecipeIngredient water = new RecipeIngredient(1,
      new Ingredient(301, "Water", Ingredient.MeasuringUnit.milliliter, Ingredient.Category.dairy,
          Ingredient.StorageType.pantry), 1000, true);

  @Override
  public void migrate() {

  }

  @Override
  public Ingredient getIngredient(int ingredientId) {
    return new Ingredient(ingredientId, "Salt", Ingredient.MeasuringUnit.teaspoon,
        Ingredient.Category.spice, Ingredient.StorageType.pantry);
  }

  @Override
  public List<Ingredient> getAllIngredients() {
    var results = new ArrayList<Ingredient>();

    results.add(carrots);
    results.add(sausage);
    results.add(battery);

    return results;
  }

  @Override
  public void updateIngredient(Ingredient newIngredient) {
  }

  @Override
  public void deleteIngredient(int ingredientId) {

  }

  @Override
  public Recipe getRecipe(int recipeId) {
    var steps = new ArrayList<String>();

    steps.add("Heat the water and stir until you see bubbles.");
    steps.add("Pour the water into the sink");
    steps.add("Down the Battery energy drink.");
    steps.add("Enjoy.");

    var ingredients = new ArrayList<RecipeIngredient>();

    ingredients.add(new RecipeIngredient(5897, battery, 500, true));

    ingredients.add(water);

    return new Recipe(recipeId, steps, Recipe.Category.dessert, Duration.ofMinutes(10),
        Recipe.Difficulty.beginner, "Battery Soup",
        "A refreshing soup made from Battery energy drink.", true, "My daily driver.", ingredients);
  }

  @Override
  public List<Recipe> getAllRecipes() {
    var recipes = new ArrayList<Recipe>();

    recipes.add(getRecipe(4));

    var steps = new ArrayList<String>();

    steps.add("Fill a pot with water.");
    steps.add("Heat the water to a boiling.");
    steps.add("Add the noodles and let them cook until soft.");
    steps.add("Pour the noodles into a bowl and enjoy.");

    var ingredients = new ArrayList<RecipeIngredient>();

    ingredients.add(new RecipeIngredient(84,
        new Ingredient(4891, "Noodles", Ingredient.MeasuringUnit.gram, Ingredient.Category.grain,
            Ingredient.StorageType.pantry), 200, true));

    ingredients.add(water);
    ingredients.add(new RecipeIngredient(5151, carrots, 1, true));

    recipes.add(new Recipe(10, steps, Recipe.Category.dinner, Duration.ofMinutes(10),
        Recipe.Difficulty.beginner, "Student's Dinner", "A meal fit for the student lifestyle.",
        false, "Boring, but cheap.", ingredients));
    recipes.add(new Recipe(142414, steps, Recipe.Category.dinner, Duration.ofMinutes(15),
        Recipe.Difficulty.beginner, "Improved Student's Dinner",
        "A more nutritious meal fit for the student lifestyle.", false,
        "Boring, but cheap. Now with carrots!", ingredients));

    return recipes;
  }

  @Override
  public void updateRecipeNote(int recipeId, String note) {
  }

  @Override
  public void updateRecipeFavorite(int recipeId, boolean isFavorite) {
  }

  @Override
  public void addIngredientItem(Ingredient ingredient, int amount, int plannedRecipeId,
                                IngredientItem.Location location) {

  }

  @Override
  public void addCustomIngredientItem(String customItemName, int amount,
                                      IngredientItem.Location location) {

  }

  @Override
  public List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location) {
    return List.of();
  }

  @Override
  public List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location,
                                                              Ingredient.StorageType storageType) {
    return List.of();
  }

  @Override
  public List<IngredientItem> getAllCustomInventoryItems() {
    ArrayList<IngredientItem> items = new ArrayList<>();
    items.add(new IngredientItem(1, "Custom Item", 1, -1, IngredientItem.Location.inventory));
    items.add(new IngredientItem(1, "Custom Item 2", 1, -1, IngredientItem.Location.inventory));
    items.add(new IngredientItem(1, "Custom Item 3", 1, -1, IngredientItem.Location.inventory));
    return items;
  }

  @Override
  public void updateIngredientItemAmount(int shoppingListItemId, int newCount) {
  }

  @Override
  public void deleteIngredientItem(int shoppingListItemId) {

  }

  @Override
  public void moveShoppingListToInventory() {
  }

  @Override
  public void moveIngredientItemToLocation(int itemId, IngredientItem.Location location) {

  }

  @Override
  public List<Ingredient> getIngredientsByStorageType(Ingredient.StorageType storageType) {
    var results = new ArrayList<Ingredient>();

    results.add(carrots);
    results.add(sausage);
    results.add(battery);

    return results;
  }

  @Override
  public void removeIngredientItemsOfRecipe(int recipeId) {

  }
}
