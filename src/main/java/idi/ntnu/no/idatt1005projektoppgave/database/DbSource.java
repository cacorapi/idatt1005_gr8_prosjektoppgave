package idi.ntnu.no.idatt1005projektoppgave.database;

/**
 * Defines a database source.
 */
public interface DbSource {
  /**
   * Get the connection URL for the database.
   * @return The connection URL
   */
  String getConnectionUrl();
}
