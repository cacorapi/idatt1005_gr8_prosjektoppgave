package idi.ntnu.no.idatt1005projektoppgave.database;

import idi.ntnu.no.idatt1005projektoppgave.model.Ingredient;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import idi.ntnu.no.idatt1005projektoppgave.model.Recipe;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Defines methods required for any database implementation.
 */
public interface Database {

  /**
   * Run database migrations.
   *
   * @throws SQLException If the database call fails for any reason
   */
  void migrate() throws FileNotFoundException, SQLException;

  /**
   * Get an ingredient by its database ID.
   *
   * @param ingredientId The ingredient's ID
   * @return The ingredient with the passed ID
   * @throws SQLException If the database call fails for any reason
   */
  Ingredient getIngredient(int ingredientId) throws SQLException;

  /**
   * Gets every ingredient in the database.
   *
   * @return A list of the ingredients in the database
   * @throws SQLException If the database call fails for any reason
   */
  List<Ingredient> getAllIngredients() throws SQLException;

  /**
   * Updates an ingredient entry.
   *
   * @param newIngredient The updated ingredient
   * @throws SQLException If the database call fails for any reason
   */
  void updateIngredient(Ingredient newIngredient) throws SQLException;

  /**
   * Deletes an ingredient with the given ID from the database.
   *
   * @param ingredientId The database ID of the ingredient to be deleted
   * @throws SQLException If the database call fails for any reason
   */
  void deleteIngredient(int ingredientId) throws SQLException;

  /**
   * Retrieves the recipe entry with the given ID.
   *
   * @param recipeId The ID of recipe to retrieve
   * @return The recipe with the given ID
   * @throws SQLException If the database call fails for any reason
   */
  Recipe getRecipe(int recipeId) throws SQLException;

  /**
   * Gets every recipe in the database.
   *
   * @return A list of the recipes in the database
   * @throws SQLException If the database call fails for any reason
   */
  List<Recipe> getAllRecipes() throws SQLException;

  /**
   * Updates the user note of the recipe with the given text.
   *
   * @param recipeId The ID of recipe to update
   * @param note     The contents of the note
   * @throws SQLException If the database call fails for any reason
   */
  void updateRecipeNote(int recipeId, String note) throws SQLException;

  /**
   * Updates the 'favorite' status of the recipe with the given ID.
   *
   * @param recipeId   The ID of recipe to update
   * @param isFavorite The new favorite status of the item
   * @throws SQLException If the database call fails for any reason
   */
  void updateRecipeFavorite(int recipeId, boolean isFavorite) throws SQLException;

  /**
   * Adds an amount of an ingredient to the inventory or the shopping list.
   *
   * @param ingredient      An instance describing the ingredient to be added
   * @param amount          The amount of the ingredient to add denoted in the ingredient's unit of measurement
   * @param plannedRecipeId The database ID of the recipe for which this ingredient is meant;
   *                        may be set to -1 if there is no recipe in particular planned for the ingredient
   * @param location        The target location for the ingredient (the shopping list or the inventory)
   * @throws SQLException If the database call fails for any reason
   */
  void addIngredientItem(Ingredient ingredient, int amount, int plannedRecipeId,
                         IngredientItem.Location location) throws SQLException;


  /**
   * Adds an amount of a custom item to the inventory or the shopping list.
   *
   * @param customItemName The custom name for the item to be added
   * @param amount         The amount of the ingredient to add
   * @param location       The target location for the ingredient (the shopping list or the inventory)
   * @throws SQLException If the database call fails for any reason
   */
  void addCustomIngredientItem(String customItemName, int amount, IngredientItem.Location location)
      throws SQLException;

  /**
   * All items stored in the inventory or on the shopping list.
   *
   * @param location The location to query (the shopping list or the inventory)
   * @return Every item stored at the target location
   * @throws SQLException If the database call fails for any reason
   */
  List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location)
      throws SQLException;

  /**
   * All items stored in the inventory or on the shopping list, filtered by storage type.
   *
   * @param location    The location to query (the shopping list or the inventory)
   * @param storageType The storage type to filter by
   * @return Every item stored at the target location
   * @throws SQLException If the database call fails for any reason
   */
  List<IngredientItem> getAllIngredientItemsOfLocation(IngredientItem.Location location,
                                                       Ingredient.StorageType storageType)
      throws SQLException;

  /**
   * All custom inventory items.
   *
   * @return Every custom inventory item
   * @throws SQLException If the database call fails for any reason
   */
  List<IngredientItem> getAllCustomInventoryItems() throws SQLException;

  /**
   * Update an inventory or shopping list item with a new amount.
   *
   * @param shoppingListItemId The ID of the item to be updated
   * @param newCount           The new amount of the item
   * @throws SQLException If the database call fails for any reason
   */
  void updateIngredientItemAmount(int shoppingListItemId, int newCount) throws SQLException;

  /**
   * Delete the inventory or shopping list item entry with the given ID from the database.
   *
   * @param shoppingListItemId The ID of the item to be deleted
   * @throws SQLException If the database call fails for any reason
   */
  void deleteIngredientItem(int shoppingListItemId) throws SQLException;

  /**
   * Moves every shopping list item into the inventory.
   *
   * @throws SQLException If the database call fails for any reason
   */
  void moveShoppingListToInventory() throws SQLException;

  /**
   * Moves a single ingredient item to another location, for example moving a shopping list item
   * to the inventory or vice versa.
   *
   * @param itemId   The id of the item to move
   * @param location The destination location
   * @throws SQLException If the database call fails for any reason
   */
  void moveIngredientItemToLocation(int itemId, IngredientItem.Location location)
      throws SQLException;

  /**
   * Get ingredients by storage type.
   *
   * @param storageType The storage type to filter by
   * @return A list of ingredients that are stored in the given storage type
   * @throws SQLException If the database call fails for any reason
   */
  List<Ingredient> getIngredientsByStorageType(Ingredient.StorageType storageType)
      throws SQLException;

  /**
   * Get all ingredients that are stored in the inventory.
   * @param recipeId The ID of the recipe to get ingredients for
   */
  void removeIngredientItemsOfRecipe(int recipeId) throws SQLException;
}

