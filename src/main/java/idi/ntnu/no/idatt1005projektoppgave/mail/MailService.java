package idi.ntnu.no.idatt1005projektoppgave.mail;

import com.resend.Resend;
import com.resend.core.exception.ResendException;
import com.resend.services.emails.model.SendEmailRequest;
import com.resend.services.emails.model.SendEmailResponse;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import java.util.List;


/**
 * Sending emails to the user.
 */
public class MailService {
  private static final String apiToken = "re_e6LujLt8_2j2vtZwHiuMtSaHQYpw6RXwV";
  private final Resend resend;

  /**
   * Initialize the mailing service.
   */
  public MailService() {
    this.resend = new Resend(apiToken);
  }

  /**
   * Send the contents of the given shopping list to the given recipient address as an email.
   *
   * @param recipient The email address of the recipient
   * @param shoppingListItems The contents of the shopping list
   */
  public void sendMail(String recipient, List<IngredientItem> shoppingListItems) {
    var sendEmailRequest =
        SendEmailRequest.builder()
            .from("Ate <no-reply@texicon.online>")
            .to(recipient)
           .subject("Your shopping list")
            .html(formatMail(shoppingListItems))
            .build();

    try {
      SendEmailResponse ignored = resend.emails().send(sendEmailRequest);
    } catch (ResendException e) {
      System.out.println(e.getMessage());
    }
  }

  private static String formatMail(List<IngredientItem> shoppingListItems) {
    StringBuilder html = new StringBuilder("<h1>Your shopping list</h1><ul>");
    for (IngredientItem item : shoppingListItems) {
      var ingredient = item.getIngredient();
      String unit = "";
      if (ingredient.isPresent()) {
        unit = ingredient.get().getUnit().name();
      }
      html.append("<li>").append(item.getName()).append(" ").append(item.getAmount()).append(" ")
          .append(unit).append("</li>");
    }

    html.append("</ul>");
    return html.toString();
  }
}
