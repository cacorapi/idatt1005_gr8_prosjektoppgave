package idi.ntnu.no.idatt1005projektoppgave.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;

/**
 * An item placed in the user's inventory.
 */
public final class IngredientItem {
  /**
   * The id/primary key of the item.
   */
  private final int id;

  /**
   * The ingredient of the item, if it is not custom.
   */
  private final Ingredient ingredient;

  /**
   * The custom name of the item, if it is custom.
   */
  private final String customName;

  /**
   * The amount of the item.
   */
  private final int amount;

  /**
   * The planned recipe of the item, if it is planned.
   */
  private final int plannedRecipeId;

  private final Location location;

  public enum Location {
    inventory,
    shoppingList
  }

  /**
   * Creates a new inventory or shopping list item using an ingredient reference.
   *
   * @param id            The id of the item.
   * @param ingredient    The ingredient of the item, if it is not custom.
   * @param amount        The amount of the item.
   * @param plannedRecipeId The id of the planned recipe of the item, if it is planned; may be null
   * @param location The location for the item (shopping list or inventory)
   */
  public IngredientItem(int id, Ingredient ingredient, int amount,
                        int plannedRecipeId, Location location) {
    this.id = id;
    this.ingredient = ingredient;
    this.customName = null;
    this.amount = amount;
    this.plannedRecipeId = plannedRecipeId;
    this.location = location;
  }

  /**
   * Creates a new inventory or shopping list item using a custom name.
   *
   * @param id            The id of the item.
   * @param customName    The custom name of the item, if it is custom.
   * @param amount        The amount of the item.
   * @param plannedRecipeId The planned recipe id of the item, if it is planned; may be null
   * @param location The location for the item (shopping list or inventory)
   */
  public IngredientItem(int id, String customName, int amount,
                        int plannedRecipeId, Location location) {
    this.id = id;
    this.ingredient = null;
    this.customName = customName;
    this.amount = amount;
    this.plannedRecipeId = plannedRecipeId;
    this.location = location;
  }

  /**
   * Generates an item from a ResultSet.
   *
   * @param result The set from which the item will be constructed
   * @return The item as it exists in the database
   * @throws SQLException If the database call fails for any reason
   */
  public static IngredientItem fromDatabase(ResultSet result) throws SQLException {
    if (result.getInt("ingredient_id") != 0) {
      return new IngredientItem(
          result.getInt("id"),
              new Ingredient(result.getInt("ingredient_id"), result.getString("name"),
                      Ingredient.MeasuringUnit.valueOf(result.getString("unit")),
                      Ingredient.Category.valueOf(result.getString("category")),
                      Ingredient.StorageType.valueOf(result.getString("storage_type"))),
          result.getInt("amount"),
          result.getInt("recipe_id"),
          Location.valueOf(result.getString("location"))
      );
    } else {
      return new IngredientItem(
          result.getInt("id"),
          result.getString("custom_name"),
          result.getInt("amount"),
          result.getInt("recipe_id"),
          Location.valueOf(result.getString("location"))
      );
    }
  }


  /**
   * Get the id.
   *
   * @return The id of the item.
   */
  public int getId() {
    return id;
  }

  /**
   * Get the ingredients.
   *
   * @return The ingredient of the item, if it is not custom.
   */
  public Optional<Ingredient> getIngredient() {
    return Optional.ofNullable(this.ingredient);
  }

  /**
   * Get the custom name.
   *
   * @return The custom name of the item, if it is custom.
   */
  @SuppressWarnings("unused")
  public Optional<String> getCustomName() {
    return Optional.ofNullable(this.customName);
  }

  /**
   * Get the amount.
   *
   * @return The amount of the item.
   */
  public int getAmount() {
    return amount;
  }

  /**
   * Get the planned recipe.
   *
   * @return The planned recipe of the item, if it is planned.
   */
  public int getPlannedRecipeId() {
    return this.plannedRecipeId;
  }

  /**
   * Get whether it is custom (custom name).
   *
   * @return True if the item is custom, false otherwise.
   */
  public boolean isCustom() {
    return this.customName != null;
  }

  /**
   * Get the name of the item, either custom or by ingredient reference.
   *
   * @return The name of the item.
   * @throws NullPointerException if neither the customName nor the ingredient is set.
   */
  public String getName() throws IllegalStateException {
    if (isCustom()) {
      return this.customName;
    } else if (this.ingredient != null) {
      return this.ingredient.getName();
    }
    throw new IllegalStateException("Both customName and ingredient were null");
  }

  public Location getLocation() {
    return location;
  }

  @Override
  public String toString() {
    return "InventoryItem{" +
        "id=" + id +
        ", ingredient=" + ingredient +
        ", customName='" + customName + '\'' +
        ", amount=" + amount +
        ", plannedRecipe=" + plannedRecipeId +
        ", location=" + location.name() +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    IngredientItem that = (IngredientItem) o;

    if (id != that.id) {
      return false;
    }
    if (amount != that.amount) {
      return false;
    }
    if (!Objects.equals(ingredient, that.ingredient)) {
      return false;
    }
    return Objects.equals(customName, that.customName);
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (ingredient != null ? ingredient.hashCode() : 0);
    result = 31 * result + (customName != null ? customName.hashCode() : 0);
    result = 31 * result + amount;
    result = 31 * result + location.hashCode();
    return result;
  }
}
