package idi.ntnu.no.idatt1005projektoppgave.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Represents a single ingredient record in the database.
 */
public final class Ingredient {
  /**
   * The primary identifier and key for this row in the database.
   */
  private final int id;

  /**
   * The name of this ingredient.
   */
  private final String name;

  /**
   * The type of measuring unit used to measure this ingredient.
   */
  private final MeasuringUnit unit;

  /**
   * The ingredient category this ingredient belongs to, used to categorize ingredients.
   */
  private final Category category;

  /**
   * Where this ingredient should be stored at home.
   */
  private final StorageType storageType;

  /**
   * Construct an ingredient object using a value for each field.
   * You may pass -1 to defaultAmount in order to set a none value.
   *
   * @param id          The primary key.
   * @param name        The name of the ingredient.
   * @param unit        The unit to use.
   * @param category    Food category.
   * @param storageType Where to store this ingredient at home.
   */
  public Ingredient(int id, String name, MeasuringUnit unit, Category category,
                    StorageType storageType) {
    this.id = id;
    this.name = name;
    this.unit = unit;
    this.category = category;
    this.storageType = storageType;
  }

  /**
   * Construct an ingredient using a ResultSet from the database
   *
   * @param result Database result to construct an ingredient from. Must come from the ingredient
   *              table.
   * @return A new ingredient object.
   * @throws SQLException If the result set is not valid, i.e. it is missing the required fields
   */
  public static Ingredient fromDatabase(ResultSet result) throws SQLException {
    return new Ingredient(result.getInt("id"), result.getString("name"),
        Ingredient.MeasuringUnit.valueOf(result.getString("unit")),
        Ingredient.Category.valueOf(result.getString("category")),
        Ingredient.StorageType.valueOf(result.getString("storage_type")));
  }

  /**
   * Gets the id.
   *
   * @return The ingredient id/primary key.
   */
  public int getId() {
    return id;
  }

  /**
   * Gets the name.
   *
   * @return The name of the ingredient.
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the measuring unit.
   *
   * @return The measuring unit used for this ingredient.
   */
  public MeasuringUnit getUnit() {
    return unit;
  }

  /**
   * Get the category.
   *
   * @return The category this ingredient belongs to.
   */
  public Category getCategory() {
    return category;
  }

  /**
   * Get the storage type.
   *
   * @return The storage type where this ingredient should be stored.
   */
  public StorageType getStorageType() {
    return storageType;
  }

  /**
   * All types of measuring units that can be used by ingredients.
   */
  public enum MeasuringUnit {
    milliliter, gram, teaspoon, spoon, piece
  }

  /**
   * Food category of an ingredient.
   */
  public enum Category {
    fruit, vegetable, meat, dairy, grain, herb, spice, oil
  }

  /**
   * All places an ingredient can be stored at home.
   */
  public enum StorageType {
    fridge, freezer, pantry
  }

  @Override
  public String toString() {
    return "Ingredient{" + "id=" + id + ", name='" + name + '\'' + ", unit=" + unit +
        ", category=" + category + ", storageType=" + storageType + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Ingredient that = (Ingredient) o;

    if (id != that.id) {
      return false;
    }
    if (!name.equals(that.name)) {
      return false;
    }
    if (unit != that.unit) {
      return false;
    }
    if (category != that.category) {
      return false;
    }
    return storageType == that.storageType;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + name.hashCode();
    result = 31 * result + unit.hashCode();
    result = 31 * result + category.hashCode();
    result = 31 * result + storageType.hashCode();
    return result;
  }
}
