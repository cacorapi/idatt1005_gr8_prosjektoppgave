package idi.ntnu.no.idatt1005projektoppgave.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.List;
import java.util.Objects;

/**
 * A single meal recipe.
 */
public final class Recipe {
  /**
   * The id/primary key of this recipe.
   */
  private final int id;

  /**
   * The path to the markdown file containing all steps for this recipe in a list format.
   */
  private final List<String> steps;

  /**
   * The meal category this recipe belongs to.
   */
  private final Category category;

  /**
   * How long it usually takes to make this recipe.
   */
  private final Duration duration;

  /**
   * How difficult it is to make this recipe.
   */
  private final Difficulty difficulty;

  /**
   * The name of this recipe.
   */
  private final String name;

  /**
   * The description of this recipe.
   */
  private final String description;

  /**
   * Whether this recipe has been marked as a favorite by the user.
   */
  private final boolean isFavorite;

  /**
   * A custom note attached to this recipe by the user.
   */
  private final String note;

  /**
   * All ingredients required to make this recipe.
   */
  private final List<RecipeIngredient> ingredients;

  /**
   * Constructs a recipe from its parts.
   *
   * @param id          The primary key of this record
   * @param steps       The steps required to make this recipe
   * @param category    The category
   * @param duration    The duration
   * @param difficulty  The difficulty
   * @param name        The name
   * @param description The description
   * @param isFavorite  Whether this is marked as favorite
   * @param note        Custom note from user
   * @param ingredients All ingredients in this recipe
   */
  public Recipe(int id, List<String> steps,
                Category category, Duration duration,
                Difficulty difficulty, String name,
                String description, boolean isFavorite, String note,
                List<RecipeIngredient> ingredients) {
    this.id = id;
    this.steps = steps;
    this.category = category;
    this.duration = duration;
    this.difficulty = difficulty;
    this.name = name;
    this.description = description;
    this.isFavorite = isFavorite;
    this.note = note;
    this.ingredients = ingredients;
  }


  /**
   * Constructs a recipe from a ResultSet and some other information.
   *
   * @param recipeId The database ID of the recipe
   * @param result The result of a database query
   * @param ingredients The ingredient required in the recipe
   * @return The recipe as it exists in the database
   * @throws SQLException If the database call failed for any reason
   */
  public static Recipe fromDatabase(int recipeId, ResultSet result,
                                    List<RecipeIngredient> ingredients) throws SQLException {
    return new Recipe(
        recipeId,
        List.of(result.getString("steps").split(";")),
        Category.valueOf(result.getString("meal_category")),
        Duration.ofMinutes(result.getInt("duration_minutes")),
        Difficulty.valueOf(result.getString("difficulty")),
        result.getString("name"),
        result.getString("description"),
        result.getBoolean("is_favorite"),
        result.getString("note"),
        ingredients
    );
  }

  /**
   * Gets the id / primary key.
   */
  public int getId() {
    return id;
  }

  /**
   * All steps required to perform this recipe, in order.
   */
  public List<String> getSteps() {
    return steps;
  }

  /**
   * The meal category.
   */
  public Category getCategory() {
    return category;
  }

  /**
   * The duration of.
   */
  @SuppressWarnings("unused")
  public Duration getDuration() {
    return duration;
  }

  /**
   * The difficulty of this recipe.
   */
  @SuppressWarnings("unused")
  public Difficulty getDifficulty() {
    return difficulty;
  }

  /**
   * The name of the recipe.
   */
  public String getName() {
    return name;
  }

  /**
   * The description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Whether this is marked as favorite by the user.
   */
  public boolean isFavorite() {
    return isFavorite;
  }

  /**
   * Custom user note on this recipe.
   */
  public String getNote() {
    return note;
  }

  /**
   * Add required ingredients to make this recipe.
   */
  public List<RecipeIngredient> getIngredients() {
    return ingredients;
  }

  /**
   * The type of meal.
   */
  public enum Category {
    any, breakfast, lunch, dinner, dessert
  }

  /**
   * The difficulty level on a recipe.
   */
  public enum Difficulty {
    beginner, intermediate, expert
  }

  @Override
  public String toString() {
    return "Recipe[" +
        "id=" + id + ", " +
        "steps=" + steps + ", " +
        "category=" + category + ", " +
        "duration=" + duration + ", " +
        "difficulty=" + difficulty + ", " +
        "name=" + name + ", " +
        "description=" + description + ", " +
        "isFavorite=" + isFavorite + ", " +
        "note=" + note + ", " +
        "ingredients=" + ingredients + ']';
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    var that = (Recipe) obj;
    return this.id == that.id &&
        Objects.equals(this.steps, that.steps) &&
        Objects.equals(this.category, that.category) &&
        Objects.equals(this.duration, that.duration) &&
        Objects.equals(this.difficulty, that.difficulty) &&
        Objects.equals(this.name, that.name) &&
        Objects.equals(this.description, that.description) &&
        this.isFavorite == that.isFavorite &&
        Objects.equals(this.note, that.note) &&
        Objects.equals(this.ingredients, that.ingredients);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, steps, category, duration, difficulty, name, description, isFavorite,
        note, ingredients);
  }
}
