package idi.ntnu.no.idatt1005projektoppgave.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * An ingredient used in a recipe, with a specific amount and necessity.
 */
public final class RecipeIngredient {
  /**
   * The id of the ingredient.
   */
  private final int id;

  /**
   * The amount of the ingredient needed in the recipe.
   */
  private final int amount;

  /**
   * Whether the ingredient is necessary for the recipe.
   */
  private final boolean isNecessary;

  /**
   * The custom name of the ingredient, if it is custom.
   */
  private final String customName;

  /**
   * The ingredient reference to use, if not custom
   */
  private final Ingredient ingredient;

  /**
   * Constructs a new RecipeIngredient using custom name
   *
   * @param id          The id of the ingredient.
   * @param customName  The name of the ingredient.
   * @param amount      The amount of the ingredient needed in the recipe.
   * @param isNecessary Whether the ingredient is necessary for the recipe.
   */
  @SuppressWarnings("unused")
  public RecipeIngredient(int id, String customName, int amount, boolean isNecessary) {
    this.id = id;
    this.amount = amount;
    this.isNecessary = isNecessary;
    this.ingredient = null;
    this.customName = customName;
  }

  /**
   * Constructs a new RecipeIngredient using an ingredient reference.
   *
   * @param id          The id of the ingredient.
   * @param ingredient  The ingredient to reference
   * @param amount      The amount of the ingredient needed in the recipe.
   * @param isNecessary Whether the ingredient is necessary for the recipe.
   */
  public RecipeIngredient(int id, Ingredient ingredient, int amount, boolean isNecessary) {
    this.id = id;
    this.customName = null;
    this.amount = amount;
    this.isNecessary = isNecessary;
    this.ingredient = ingredient;
  }


  /**
   * Constructs a recipe ingredient from a ResultSet.
   *
   * @param result The result of the query from with to construct the ingredient
   * @return The recipe ingredient as it exists in the database
   * @throws SQLException If the database call failed for any reason
   */
  public static RecipeIngredient fromDatabase(ResultSet result) throws SQLException {
    int id = result.getInt("id");
    int amount = result.getInt("amount");
    boolean isNecessary = result.getBoolean("is_necessary");

      Ingredient ingredient = Ingredient.fromDatabase(result);
      return new RecipeIngredient(id, ingredient, amount, isNecessary);
  }


  /**
   * Returns the amount of the ingredient needed in the recipe.
   */
  public int getAmount() {
    return amount;
  }

  /**
   * Whether this ingredient is necessary for the recipe.
   */
  @SuppressWarnings("unused")
  public boolean isNecessary() {
    return isNecessary;
  }

  /**
   * The database ID of the recipe ingredient.
   */
  public int getId() {
    return id;
  }

  /**
   * The custom name of the ingredient, if it is custom.
   */
  @SuppressWarnings("unused")
  public Optional<String> getCustomName() {
    return Optional.ofNullable(customName);
  }

  /**
   * The database ingredient this recipe ingredient refers to, if it is not custom.
   */
  public Optional<Ingredient> getIngredient() {
    return Optional.ofNullable(ingredient);
  }

  /**
   * The name of the database ingredient this recipe ingredient refers to, if it is not custom.
   */
  public String getName() {
    if (customName != null) {
      return customName;
    } else {
      assert ingredient != null : "Invalid state: customName and ingredient are both null.";
      return ingredient.getName();
    }
  }

  @Override
  public String toString() {
    return "RecipeIngredient{" +
        "amount=" + amount +
        ", isNecessary=" + isNecessary +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }

    RecipeIngredient that = (RecipeIngredient) o;

    if (amount != that.amount) {
      return false;
    }
    return isNecessary == that.isNecessary;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + amount;
    result = 31 * result + (isNecessary ? 1 : 0);
    return result;
  }
}
