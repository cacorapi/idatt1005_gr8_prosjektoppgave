package idi.ntnu.no.idatt1005projektoppgave.view.groupedIngredientItemCard;

import idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard.IngredientItemCardHandler;

/**
 * Handles all events triggered by the GroupedIngredientItemCard
 * ALso requires methods of IngredientItemCardHandler to handle events triggered by the child
 * cards of the component
 */
public interface GroupedIngredientItemCardHandler extends IngredientItemCardHandler {
  /**
   * Handles the event of removing a planned recipe
   *
   * @param plannedRecipeId the id of the planned recipe to remove
   */
  void onRemovePlannedRecipe(int plannedRecipeId);
}
