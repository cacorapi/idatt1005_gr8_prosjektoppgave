package idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard;

import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;

/**
 * The card for displaying an ingredient in the shopping list or the inventory
 */
public class IngredientItemCard extends HBox {
  private final IngredientItemCardHandler handler;
  private final IngredientItem ingredientItem;

  /**
   * Create an item card from a handler and an item.
   *
   * @param handler The handler to use for the card
   * @param ingredientItem The item
   */
  public IngredientItemCard(IngredientItemCardHandler handler, IngredientItem ingredientItem) {
    this.handler = handler;
    this.ingredientItem = ingredientItem;
    initialize();
  }

  private void initialize() {
    // Create the title label
    Label titleLabel = new Label(ingredientItem.getName());

    // Spacer to push everything to the sides
    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);

    // Control container
    HBox controlBox = new HBox();
    controlBox.setAlignment(Pos.CENTER);
    controlBox.setSpacing(5);

    // Decrement button
    Button decrementButton = new Button("-");
    decrementButton.setOnAction(event -> {
      if (ingredientItem.getAmount() > 1) {
        changeAmount(-1);
      } else {
        handler.onIngredientItemDelete(ingredientItem);
      }
    });

    // Quantity text
    Text quantityText = new Text(String.valueOf(ingredientItem.getAmount()));

    // Increment button
    Button incrementButton = new Button("+");
    incrementButton.setOnAction(e -> changeAmount(1));

    // Add buttons and quantity text to control container
    controlBox.getChildren().addAll(decrementButton, quantityText, incrementButton);

    // Delete button with bin icon
    Button deleteButton = new Button("\uD83D\uDDD1");
    deleteButton.setOnAction(event -> handler.onIngredientItemDelete(ingredientItem));

    // Add move individual to inventory button (for shopping list)
    if (ingredientItem.getLocation() == IngredientItem.Location.shoppingList) {
      Button moveToInventoryButton = new Button("←");
      moveToInventoryButton.setOnAction(event -> handler.moveIngredientItem(ingredientItem));
      getChildren().add(moveToInventoryButton);
    }

    // HBox container for the card
    setSpacing(10);
    setAlignment(Pos.CENTER_LEFT);
    getChildren().addAll(titleLabel, spacer);

    if (ingredientItem.getIngredient().isPresent()) {
      getChildren().add(new Label(ingredientItem.getIngredient().get().getUnit().toString()));
    }

    getChildren().addAll(controlBox, deleteButton);
  }

  private void changeAmount(int multiplier) {
    int amount = getIncrementSizeForIngredient(ingredientItem) * multiplier;

    handler.onChangeIngredientItemAmount(ingredientItem, ingredientItem.getAmount() + amount);
  }

  private int getIncrementSizeForIngredient(IngredientItem item) {
    if (item.getIngredient().isPresent()) {
      switch (item.getIngredient().get().getUnit()) {
        case milliliter, gram -> {
          return 10;
        }
        case teaspoon, spoon, piece -> {
          return 1;
        }
      }
      throw new IllegalArgumentException("Invalid unit");
    } else {
      return 1;
    }
  }
}
