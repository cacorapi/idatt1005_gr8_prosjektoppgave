package idi.ntnu.no.idatt1005projektoppgave.view.groupedIngredientItemCard;

import idi.ntnu.no.idatt1005projektoppgave.database.Database;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import idi.ntnu.no.idatt1005projektoppgave.model.Recipe;
import idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard.IngredientItemCard;
import java.sql.SQLException;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;


/**
 * A group for an ingredient item card.
 */
public class GroupedIngredientItemCard extends VBox {
  final List<IngredientItem> items;
  final int recipeId;
  final Database database;
  final GroupedIngredientItemCardHandler handler;

  /**
   * Creates a group from its items and a recipe and attaches a handler.
   *
   * @param items The items to be grouped
   * @param recipeId The database ID of the recipe the ingredients will be grouped into
   * @param database The database implementation to use
   * @param handler The event handler
   */
  public GroupedIngredientItemCard(List<IngredientItem> items, int recipeId,
                                   Database database, GroupedIngredientItemCardHandler handler) {
    this.items = items;
    this.recipeId = recipeId;
    this.database = database;
    this.handler = handler;
    initialize();
  }


  private void initialize() {
    Recipe recipe = null;
    String recipeName;
    if (recipeId > 0) {
      try {
        recipe  = database.getRecipe(recipeId);
      } catch (SQLException e) {
        System.out.println(e.getMessage());
      }

      if (recipe != null) {
        recipeName = recipe.getName();
      } else {
        recipeName = "Uncategorized";
      }
    } else {
      recipeName = "Uncategorized";
    }

    // Create the title label
    Label titleLabel = new Label(recipeName);
    titleLabel.setStyle("-fx-font-size: 16; -fx-font-weight: bold; -fx-text-fill: #000000");

    HBox titleBox = new HBox();

    titleBox.getChildren().add(titleLabel);

    if (recipe != null) {
      // Delete button with bin icon
      Button deleteButton = new Button("\uD83D\uDDD1");
      Recipe finalPlannedRecipe = recipe;
      deleteButton.setOnAction(event -> handler.onRemovePlannedRecipe(finalPlannedRecipe.getId()));

      Region spacer = new Region();
      HBox.setHgrow(spacer, Priority.ALWAYS);

      titleBox.getChildren().addAll(spacer, deleteButton);
    }


    setSpacing(5);

    getChildren().add(titleBox);

    items.forEach(item -> getChildren().add(new IngredientItemCard(handler, item)));
  }
}
