package idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard;

import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;

/**
 * Handles all events triggered by the IngredientItemCard
 */
public interface IngredientItemCardHandler {
  /**
   * Changes the amount of an ingredient item.
   *
   * @param item   The item to change the amount of
   * @param amount The amount to change it by
   */
  void onChangeIngredientItemAmount(IngredientItem item, int amount);

  /**
   * Deletes an ingredient item from the inventory or shopping list.
   *
   * @param item The item to delete
   */
  void onIngredientItemDelete(IngredientItem item);

  /**
   * Moves an ingredient item to another location. I.e. if it is in the shopping list, it gets
   * moved to the inventory, and vice versa.
   *
   * @param item The item to move
   */
  void moveIngredientItem(IngredientItem item);
}
