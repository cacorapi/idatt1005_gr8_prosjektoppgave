package idi.ntnu.no.idatt1005projektoppgave;

import idi.ntnu.no.idatt1005projektoppgave.database.Database;
import idi.ntnu.no.idatt1005projektoppgave.database.LocalDbSource;
import idi.ntnu.no.idatt1005projektoppgave.database.SqliteDatabase;
import idi.ntnu.no.idatt1005projektoppgave.database.TestingDbSource;
import idi.ntnu.no.idatt1005projektoppgave.mail.MailService;
import idi.ntnu.no.idatt1005projektoppgave.model.Ingredient;
import idi.ntnu.no.idatt1005projektoppgave.model.IngredientItem;
import idi.ntnu.no.idatt1005projektoppgave.model.Recipe;
import idi.ntnu.no.idatt1005projektoppgave.model.RecipeIngredient;
import idi.ntnu.no.idatt1005projektoppgave.view.confirmDialog.ConfirmDialogBuilder;
import idi.ntnu.no.idatt1005projektoppgave.view.groupedIngredientItemCard.GroupedIngredientItemCard;
import idi.ntnu.no.idatt1005projektoppgave.view.groupedIngredientItemCard.GroupedIngredientItemCardHandler;
import idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard.IngredientItemCard;
import idi.ntnu.no.idatt1005projektoppgave.view.ingredientItemCard.IngredientItemCardHandler;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Controls the state of the home page.
 */
public class AppController implements IngredientItemCardHandler, GroupedIngredientItemCardHandler {
  private final Database database;
  private final MailService mailService;
  @FXML
  private Button AddItemToShoppingList;
  @FXML
  private Button RemoveItemFromList;
  @FXML
  private Button AddToInventory;
  @FXML
  private Button ExportList;
  @FXML
  private ScrollPane ShoppingListScroll;
  public StackPane customStackPane;
  public CheckBox groupByRecipeCheck;
  @FXML
  private Button AddItemToInventory;
  @FXML
  private StackPane pantryStackPane;
  @FXML
  private StackPane fridgeStackPane;
  @FXML
  private StackPane freezerStackPane;
  @FXML
  private TabPane recipeCategoryTabs;
  @FXML
  private ListView<String> favouriteRecipesList;
  @FXML
  private ListView<String> allRecipesList;
  @FXML
  private ListView<String> breakfastRecipesList;
  @FXML
  private ListView<String> lunchRecipesList;
  @FXML
  private ListView<String> dinnerRecipesList;
  @FXML
  private ListView<String> dessertRecipesList;
  @FXML
  private Label recipeOverview;
  @FXML
  private CheckBox favCheckBox;
  private int selectedRecipe;
  private ArrayList<Recipe> displayedRecipes;
  private ObservableList<String> recipeList;
  @FXML
  private Button addRecipeToShoppingList;
  @FXML
  private TextField recipeNote;
  @FXML
  private Button recipeNoteSave;

  @FXML
  private void initialize() {
    try {
      refreshInventory();
      setUpHomePageButtons();
      refreshShoppingList();
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new RuntimeException(e);
    }

    recipeOverview.setWrapText(true);

    var lists = new ArrayList<ListView<String>>();

    lists.add(favouriteRecipesList);
    lists.add(allRecipesList);
    lists.add(breakfastRecipesList);
    lists.add(lunchRecipesList);
    lists.add(dinnerRecipesList);
    lists.add(dessertRecipesList);

    for (var list : lists) {
      list.getSelectionModel().selectedIndexProperty().addListener(((observableValue, s, t1) -> {
        var idx = t1.intValue();
        if (idx != -1) {
          selectedRecipe = idx;
          onRecipeClick();
        }
      }));
    }

    recipeNoteSave.setOnAction(this::onRecipeNoteSave);

    displayedRecipes = new ArrayList<>();
    selectedRecipe = -1;

    groupByRecipeCheck.selectedProperty().addListener(((observableValue, aBoolean, t1) -> {
      try {
        refreshShoppingList();
        refreshInventory();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    }));


    favCheckBox.selectedProperty().addListener(((observableValue, aBoolean, t1) -> {
      if (selectedRecipe != -1) {
        try {
          var maybeRecipe = getSelectedRecipe();
          if (maybeRecipe.isPresent()) {
            var recipe = maybeRecipe.get();
            if (recipe.isFavorite() != t1) {
              database.updateRecipeFavorite(recipe.getId(), t1);
            }
          }
        } catch (SQLException e) {
          throw new RuntimeException(e);
        }
      }
    }));

    addRecipeToShoppingList.setOnAction(actionEvent -> {
      try {
        var maybeSelected = getSelectedRecipe();
        if (maybeSelected.isPresent()) {
          onAddAllToShoppingList(maybeSelected.get().getId());
        }
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    });

    this.recipeList = FXCollections.observableArrayList();
    this.favouriteRecipesList.setItems(this.recipeList);
    this.allRecipesList.setItems(this.recipeList);
    this.breakfastRecipesList.setItems(this.recipeList);
    this.lunchRecipesList.setItems(this.recipeList);
    this.dinnerRecipesList.setItems(this.recipeList);
    this.dessertRecipesList.setItems(this.recipeList);

    recipeCategoryTabs.getSelectionModel().selectedIndexProperty()
        .addListener(((observableValue, tab, t1) -> {
          Recipe.Category category = Recipe.Category.any;
          ListView<String> toRefresh = allRecipesList;
          switch (t1.intValue()) {
            case 0 -> {
              onFavouriteCategorySelect();
              favouriteRecipesList.refresh();
              return;
            }
            case 2 -> {
              category = Recipe.Category.breakfast;
              toRefresh = breakfastRecipesList;
            }
            case 3 -> {
              category = Recipe.Category.lunch;
              toRefresh = lunchRecipesList;
            }
            case 4 -> {
              category = Recipe.Category.dinner;
              toRefresh = dinnerRecipesList;
            }
            case 5 -> {
              category = Recipe.Category.dessert;
              toRefresh = dessertRecipesList;
            }
            default -> {
            }
          }
          try {
            onRecipeCategorySelect(category);
            toRefresh.refresh();
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
        }));
    recipeCategoryTabs.getSelectionModel().select(0);
    recipeCategoryTabs.getSelectionModel().select(1);
  }

  private Optional<Recipe> getSelectedRecipe() {
    if (0 <= selectedRecipe && selectedRecipe < this.displayedRecipes.size()) {
      return Optional.of(this.displayedRecipes.get(selectedRecipe));
    }
    return Optional.empty();
  }

  private void refetchSelectedRecipe() {
    getSelectedRecipe().ifPresent(selected -> {
      try {
        this.displayedRecipes.set(selectedRecipe, database.getRecipe(selected.getId()));
      } catch (SQLException ignored) {

      }
    });
  }


  /**
   * Constructs the app controller and its dependencies.
   *
   * @throws SQLException If no connection can be established with the database
   * @throws ClassNotFoundException If the JDBC driver class cannot be found
   */
  public AppController() throws SQLException, ClassNotFoundException {
    this.mailService = new MailService();
    this.database = new SqliteDatabase(new TestingDbSource());
  }

  @FXML
  private void onAddToShoppingListClick() throws SQLException {
    addToShoppingListPopup();
    refreshShoppingList();
  }

  @FXML
  private void addToShoppingListPopup() {
    Stage popUp = new Stage();
    popUp.initModality(Modality.APPLICATION_MODAL);

    Text requestMessage = new Text("Write name of ingredient to add to shopping list");
    requestMessage.setTextAlignment(TextAlignment.CENTER);
    TextField inputIngredient = new TextField();
    inputIngredient.setMaxWidth(125);

    Button btnAdd = new Button("Add");
    Button btnCancel = new Button("Cancel");

    HBox containerBtn = new HBox(btnAdd, btnCancel);
    containerBtn.setAlignment(Pos.CENTER);

    btnAdd.setOnAction(actionEvent -> {
      IngredientItem.Location location = IngredientItem.Location.shoppingList;
      try {
        int ingredientToAddID = getIngredientIdFromInput(inputIngredient.getText());

        if (ingredientToAddID != -1) {
          onAddIngredientToIngredientItemClick(ingredientToAddID, location);
        } else {
          onAddCustomToIngredientItem(inputIngredient.getText(), location);
        }
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
      popUp.close();

    });
    btnCancel.setOnAction(actionEvent -> popUp.close());

    VBox showBox = new VBox(10);
    showBox.getChildren().addAll(requestMessage, inputIngredient, containerBtn);
    showBox.setAlignment(Pos.CENTER);

    Scene popupScene = new Scene(showBox, 350, 150);

    popUp.setScene(popupScene);
    popUp.showAndWait();
  }

  @FXML
  private int getIngredientIdFromInput(String inputIngredient) throws SQLException {
    return database.getAllIngredients().stream()
        .filter(ingredient -> ingredient.getName().equalsIgnoreCase(inputIngredient.trim()))
        .mapToInt(Ingredient::getId).findFirst().orElse(-1);
  }

  @FXML
  private void onAddCustomToIngredientItem(String customName, IngredientItem.Location location)
      throws SQLException {
    int initialSetAmount = 1;

    database.addCustomIngredientItem(customName.trim(), initialSetAmount, location);
  }


  @FXML
  private void onMoveShoppingListToInventory() throws SQLException {
    new ConfirmDialogBuilder()
        .setPrompt("Move all items to inventory?")
        .setConfirmButtonText("Move")
        .setCancelButtonText("Cancel")
        .setOnConfirm(() -> {
          try {
            database.moveShoppingListToInventory();
            refreshShoppingList();
            refreshInventory();
          } catch (SQLException e) {
            throw new RuntimeException(e);
          }
        }).createConfirmDialog()
        .show();
  }

  @FXML
  private void onDeleteShoppingList() throws SQLException {
    new ConfirmDialogBuilder().setPrompt("Delete all items in shopping list?")
        .setCancelButtonText("Cancel").setConfirmButtonText("Delete").setOnConfirm(() -> {
          try {
            deleteEveryShoppingListItem();
            refreshShoppingList();
          } catch (SQLException e) {
            throw new RuntimeException(e);
          }
        }).createConfirmDialog().show();
  }

  @FXML
  private void deleteEveryShoppingListItem() throws SQLException {
    database.getAllIngredientItemsOfLocation(IngredientItem.Location.shoppingList).stream()
        .map(IngredientItem::getId).forEach(itemId -> {
          try {
            database.deleteIngredientItem(itemId);
          } catch (SQLException e) {
            throw new RuntimeException(e);
          }
        });
  }

  @FXML
  private void onSendShoppingListToMobile() {
    Stage popUp = new Stage();
    popUp.initModality(Modality.APPLICATION_MODAL);

    TextField inputEmail = new TextField();

    inputEmail.setPromptText("Enter email");
    inputEmail.setMaxWidth(200);
    Button btnReturn = new Button("Go back");
    Button btnSend = new Button("Send me an email");

    HBox containerBtn = new HBox(btnSend, btnReturn);
    containerBtn.setSpacing(10);
    containerBtn.setAlignment(Pos.CENTER);

    btnReturn.setOnAction(actionEvent -> popUp.close());
    btnSend.setOnAction(actionEvent -> {
      try {
        mailService.sendMail(inputEmail.getText(),
            database.getAllIngredientItemsOfLocation(IngredientItem.Location.shoppingList));
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
      popUp.close();

    });

    VBox showBox = new VBox(20);
    showBox.getChildren().addAll(inputEmail, containerBtn);
    showBox.setAlignment(Pos.CENTER);

    Scene popupScene = new Scene(showBox, 350, 150);

    popUp.setScene(popupScene);
    popUp.showAndWait();
  }

  @Override
  public void onChangeIngredientItemAmount(IngredientItem item, int amount) {
    try {
      if (amount <= 0) {
        database.deleteIngredientItem(item.getId());
      } else {
        database.updateIngredientItemAmount(item.getId(), amount);
      }

      if (item.getLocation() == IngredientItem.Location.inventory) {
        refreshInventory();
      } else {
        refreshShoppingList();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onIngredientItemDelete(IngredientItem item) {
    try {
      database.deleteIngredientItem(item.getId());
      if (item.getLocation() == IngredientItem.Location.inventory) {
        refreshInventory();
      } else {
        refreshShoppingList();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private void refreshInventory() throws SQLException {
    var fridgeItems = database.getAllIngredientItemsOfLocation(IngredientItem.Location.inventory,
        Ingredient.StorageType.fridge);
    var freezerItems = database.getAllIngredientItemsOfLocation(IngredientItem.Location.inventory,
        Ingredient.StorageType.freezer);
    var pantryItems = database.getAllIngredientItemsOfLocation(IngredientItem.Location.inventory,
        Ingredient.StorageType.pantry);
    var customItems = database.getAllCustomInventoryItems();

    fridgeStackPane.getChildren().addAll(getIngredientItemCards(fridgeItems));
    freezerStackPane.getChildren().addAll(getIngredientItemCards(freezerItems));
    pantryStackPane.getChildren().addAll(getIngredientItemCards(pantryItems));
    customStackPane.getChildren().addAll(getIngredientItemCards(customItems));
  }

  private ListView<Node> getIngredientItemCards(List<IngredientItem> items) {
    final ListView<Node> inventoryItemCards = new ListView<>();

    if (groupByRecipeCheck.isSelected()) {
      items.stream().collect(Collectors.groupingBy(IngredientItem::getPlannedRecipeId)).forEach(
          (maybeRecipe, groupedItems) -> inventoryItemCards.getItems()
              .add(new GroupedIngredientItemCard(groupedItems, maybeRecipe, database, this)));
    } else {
      items.forEach(shoppingListItem -> inventoryItemCards.getItems()
          .add(new IngredientItemCard(this, shoppingListItem)));
    }

    return inventoryItemCards;
  }

  private void onAddToInventoryClick() throws SQLException {
    addToInventoryPopup();
    refreshInventory();
  }

  private void addToInventoryPopup() {
    Stage popUp = new Stage();
    popUp.initModality(Modality.APPLICATION_MODAL);

    Text requestMessage = new Text("Write name of ingredient to add to inventory");
    requestMessage.setTextAlignment(TextAlignment.CENTER);
    TextField inputIngredient = new TextField();
    inputIngredient.setMaxWidth(125);

    Button btnAdd = new Button("Add");
    Button btnCancel = new Button("Cancel");

    HBox containerBtn = new HBox(btnAdd, btnCancel);
    containerBtn.setAlignment(Pos.CENTER);

    btnAdd.setOnAction(actionEvent -> {
      IngredientItem.Location location = IngredientItem.Location.inventory;
      try {
        int ingredientToAddID = getIngredientIdFromInput(inputIngredient.getText());

        if (ingredientToAddID != -1) {
          onAddIngredientToIngredientItemClick(ingredientToAddID, location);
        } else {
          onAddCustomToIngredientItem(inputIngredient.getText(), location);
        }
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
      popUp.close();

    });
    btnCancel.setOnAction(actionEvent -> popUp.close());

    VBox showBox = new VBox(10);
    showBox.getChildren().addAll(requestMessage, inputIngredient, containerBtn);
    showBox.setAlignment(Pos.CENTER);

    Scene popupScene = new Scene(showBox, 350, 150);

    popUp.setScene(popupScene);
    popUp.showAndWait();
  }

  private void onAddIngredientToIngredientItemClick(int itemId, IngredientItem.Location location)
      throws SQLException {
    Ingredient ingredient = database.getIngredient(itemId);
    int initialAmount;

    switch (ingredient.getUnit()) {
      case milliliter, gram -> initialAmount = 10;
      default -> initialAmount = 1;
    }

    database.addIngredientItem(ingredient, initialAmount, -1, location);
  }

  private void setUpHomePageButtons() {
    AddItemToInventory.setOnAction(actionEvent -> {
      try {
        onAddToInventoryClick();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    });
    AddItemToShoppingList.setOnAction(actionEvent -> {
      try {
        onAddToShoppingListClick();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    });
    RemoveItemFromList.setOnAction(actionEvent -> {
      try {
        onDeleteShoppingList();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    });
    AddToInventory.setOnAction(actionEvent -> {
      try {
        onMoveShoppingListToInventory();
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    });
    ExportList.setOnAction(actionEvent -> onSendShoppingListToMobile());
  }

  private void refreshShoppingList() throws SQLException {
    var shoppingListItems =
        database.getAllIngredientItemsOfLocation(IngredientItem.Location.shoppingList);

    updateShoppingListPane(ShoppingListScroll, shoppingListItems);
  }

  private void updateShoppingListPane(ScrollPane view, List<IngredientItem> shoppingListItems) {
    final ListView<Node> shoppingListItemCards = new ListView<>();
    shoppingListItemCards.setPrefWidth(ShoppingListScroll.getPrefWidth());
    shoppingListItemCards.setPrefHeight(ShoppingListScroll.getPrefHeight());

    shoppingListItemCards.getItems().addAll(getIngredientItemCards(shoppingListItems));
    view.setContent(shoppingListItemCards);
  }

  @Override
  public void onRemovePlannedRecipe(int recipeId) {
    try {
      database.removeIngredientItemsOfRecipe(recipeId);
      refreshInventory();
      refreshShoppingList();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void moveIngredientItem(IngredientItem item) {
    var newLocation = item.getLocation() == IngredientItem.Location.inventory ?
        IngredientItem.Location.shoppingList : IngredientItem.Location.inventory;

    try {
      database.moveIngredientItemToLocation(item.getId(), newLocation);
      refreshInventory();
      refreshShoppingList();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private void onRecipeClick() {

    getSelectedRecipe().ifPresent(selected -> {
      favCheckBox.setSelected(selected.isFavorite());

      recipeOverview.setText((selected.getDescription() + "\r\n\r\n" + "Ingredients:\n" +
          selected.getIngredients().stream().map(e -> e.getName() + "\n")
              .collect(Collectors.joining()) + "\r\n" + selected.getSteps() + "\r\n").replace(", ",
          "\n").replace("[", "").replace("]", ""));

      recipeNote.setText(selected.getNote());
    });
  }

  private Stream<Recipe> recipesWithCategory(Recipe.Category category) {
    try {
      var recipes = this.database.getAllRecipes();
      return recipes.stream()
          .filter(r -> r.getCategory() == category || category == Recipe.Category.any);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  private void onFavouriteCategorySelect() {
    var favourites = recipesWithCategory(Recipe.Category.any).filter(Recipe::isFavorite);
    showRecipes(favourites);
  }

  private void onRecipeCategorySelect(Recipe.Category category) {
    var filtered = recipesWithCategory(category);
    showRecipes(filtered);
  }

  private void showRecipes(Stream<Recipe> newRecipes) {
    displayedRecipes.clear();
    newRecipes.forEach(displayedRecipes::add);
    var asNames = displayedRecipes.stream().map(Recipe::getName);
    recipeList.clear();
    asNames.forEach(recipeList::add);
    recipeList.add(" ");

    clearRecipeDetails();
  }

  private void clearRecipeDetails() {
    recipeOverview.setText("");
    recipeNote.setText("");
  }

  private void onAddAllToShoppingList(int recipeId) throws SQLException {
    var recipe = this.database.getRecipe(recipeId);
    var inventory =
        this.database.getAllIngredientItemsOfLocation(IngredientItem.Location.inventory);
    var shoppingList =
        this.database.getAllIngredientItemsOfLocation(IngredientItem.Location.shoppingList);

    for (var inRecipe : recipe.getIngredients()) {

      var maybeIngredient = inRecipe.getIngredient();

      if (maybeIngredient.isPresent()) {
        addIngredientToShoppingListIfNotPresent(recipeId, inRecipe, maybeIngredient.get(),
            inventory, shoppingList);
      } else {
        addCustomItemToShoppingListIfNotPresent(inRecipe, inventory, shoppingList);
      }
    }

    refreshShoppingList();
  }

  private void addIngredientToShoppingListIfNotPresent(int recipeId, RecipeIngredient inRecipe,
                                                       Ingredient ingredient,
                                                       List<IngredientItem> inventory,
                                                       List<IngredientItem> shoppingList)
      throws SQLException {
    var present = 0;


    present += getPresentAmount(ingredient, inventory.iterator());
    present += getPresentAmount(ingredient, shoppingList.iterator());

    var missing = inRecipe.getAmount() - present;
    if (missing > 0) {
      this.database.addIngredientItem(ingredient, missing, recipeId,
          IngredientItem.Location.shoppingList);
    }
  }

  private static int getPresentAmount(Ingredient ingredient,
                                      Iterator<IngredientItem> items) {
    while (items.hasNext()) {
      var item = items.next();
      var checkId = item.getIngredient().map(i -> i.getId() == ingredient.getId());
      if (checkId.orElseGet(() -> Objects.equals(item.getName(), ingredient.getName()))) {
        return item.getAmount();
      }
    }
    return 0;
  }

  private void addCustomItemToShoppingListIfNotPresent(RecipeIngredient inRecipe,
                                                       List<IngredientItem> inventory,
                                                       List<IngredientItem> shoppingList)
      throws SQLException {
    var name = inRecipe.getName();
    var present = 0;

    for (var inInventory : inventory) {
      if (Objects.equals(inInventory.getName(), name)) {
        present += inInventory.getAmount();
      }
    }

    for (var inShoppingList : shoppingList) {
      if (Objects.equals(inShoppingList.getName(), name)) {
        present += inShoppingList.getAmount();
      }
    }

    var missing = inRecipe.getAmount() - present;
    if (missing > 0) {
      this.database.addCustomIngredientItem(inRecipe.getName(), missing,
          IngredientItem.Location.shoppingList);
    }
  }

  private void onRecipeNoteSave(Event actionEvent) {
    var maybeSelected = this.getSelectedRecipe();

    System.out.println("Saving recipe note...");

    maybeSelected.ifPresent(selected -> {
      try {
        database.updateRecipeNote(selected.getId(), recipeNote.getText());
        System.out.println("Recipe note saved.");
        refetchSelectedRecipe();
      } catch (SQLException e) {
        System.out.println(
            "Error attempting to update recipe database entry with new note: " + e.getMessage());
      }
    });
  }
}
