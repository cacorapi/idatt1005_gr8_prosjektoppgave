# IDATT1005 Software Development Project

This project is a cross-platform desktop application to manage your inventory, shopping list and recipes.

## Tech

The project is built using JavaFX, the Maven package manager and Java programming language. We use java version 21 (LTS).

## Building the project

Make sure Maven and Java is installed on the system. Clone the repository and run
```bash
mvn clean install && mavn javafx:javafx
```

This will launch the application into its own window.

## Testing the project

To test the project, run
```bash
mvn test
```

Unit tests should be run on your system after the previous command.
